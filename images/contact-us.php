<?php 
/*Template Name:Contact Us*/
get_header();
?>
<style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<section class="inner-banner" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
		<div class="container">
			<div class="inner_heading">
				<h2><?php the_title();?></h2>
			</div>
		</div>
	</section>
	<section class="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-12">
					<div class="contact_form">
						<h3><?php the_title();						echo $img = get_option( 'woocommerce_email_header_image' );						?></h3>
						<?php echo do_shortcode('[contact-form-7 id="49" title="Contact Page"]');?>
					</div>
				</div>
				<?php 
				$phone = get_field('phone');
				$phone_t = str_replace(' ', '',$phone);
				?>
				<div class="col-md-6 col-sm-6 col-12">
					<div class="address_contact">
						<h3>Address</h3>
						<ul>
							<li>
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<?php the_field( 'locaton' ); ?>
							</li>
							<li>
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<a href="mailto:<?php the_field( 'email' ); ?>"><?php the_field( 'email' ); ?></a>
							</li>
							<li>
								<i class="fa fa-mobile-phone" aria-hidden="true"></i>
								<a href="tel:<?php echo $phone_t; ?>"><?php the_field( 'phone' ); ?></a>
							</li>
						</ul>
						<?php $location = get_field('map'); ?>
						<div class="acf-map map">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC64hcvgoH0eLHQ8M8yHfYeqvfxwpHAt2k"></script>
<script type="text/javascript">
(function($) {

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}


function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}
function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}


var map = null;

jQuery(document).ready(function(){

	jQuery('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>
<?php 
get_footer();
?>
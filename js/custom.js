/* Animated-header- */
jQuery(window).scroll(function() {
	var sc = $(window).scrollTop()
	if (sc > 100) {
		$("#header-sroll").addClass("small")
	} else {
		$("#header-sroll").removeClass("small")
	}
});


jQuery('.firstCap').on('keypress', function(event) {
	var $this = $(this),
		thisVal = $this.val(),
		FLC = thisVal.slice(0, 1).toUpperCase();
	con = thisVal.slice(1, thisVal.length);
	jQuery(this).val(FLC + con);
});

jQuery(document).ready(function() {
	var owl = $('.owl-carousel.testi');
	owl.owlCarousel({
		loop: true,
		nav: true,
		margin: 10,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			960: {
				items: 1
			},
			1200: {
				items: 1
			}
		}
	});
	owl.on('mousewheel', '.owl-stage', function(e) {
		if (e.deltaY > 0) {
			owl.trigger('next.owl');
		} else {
			owl.trigger('prev.owl');
		}
		e.preventDefault();
	});
})


/* Card-Checkout-Section */
jQuery('.input-cart-number').on('keyup change', function(){
  $t = $(this);

  if ($t.val().length > 3) {
    $t.next().focus();
  }

  var card_number = '';
  jQuery('.input-cart-number').each(function(){
    card_number += $(this).val() + ' ';
    if ($(this).val().length == 4) {
      jQuery(this).next().focus();
    }
  })

  jQuery('.credit-card-box .number').html(card_number);
});

jQuery('#card-holder').on('keyup change', function(){
  $t = $(this);
  jQuery('.credit-card-box .card-holder div').html($t.val());
});

jQuery('#card-holder').on('keyup change', function(){
  $t = $(this);
  jQuery('.credit-card-box .card-holder div').html($t.val());
});

jQuery('#card-expiration-month, #card-expiration-year').change(function(){
  m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
  m = (m < 10) ? '0' + m : m;
  y = $('#card-expiration-year').val().substr(2,2);
  jQuery('.card-expiration-date div').html(m + '/' + y);
})

jQuery('#card-ccv').on('focus', function(){
jQuery('.credit-card-box').addClass('hover');
}).on('blur', function(){
  jQuery('.credit-card-box').removeClass('hover');
}).on('keyup change', function(){
  jQuery('.ccv div').html($(this).val());
});

setTimeout(function(){
jQuery('#card-ccv').focus().delay(1000).queue(function(){
    jQuery(this).blur().dequeue();
  });
}, 500);
